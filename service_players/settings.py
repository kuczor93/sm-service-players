# Flask settings
FLASK_SERVER_NAME_DEV = 'localhost:9092'
FLASK_SERVER_NAME_PROD = '192.168.1.52:9090'
FLASK_DEBUG = True  # Do not use debug mode in production

# Flask-Restplus settings
RESTPLUS_SWAGGER_UI_DOC_EXPANSION = 'list'
RESTPLUS_VALIDATE = True
RESTPLUS_MASK_SWAGGER = False
RESTPLUS_ERROR_404_HELP = False

# MongoDB settings
MONGODB_DBNAME = 'statistics_manager'
MONGODB_URI = 'mongodb://192.168.1.50:27017/' + MONGODB_DBNAME
