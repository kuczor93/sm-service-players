class PlayersResponse(object):
    def __init__(self, players_added = 0, players_removed = 0, players_updated = 0):
        self.Added = players_added
        self.Removed = players_removed
        self.Updated = players_updated

    def __repr__(self):
        return {
            'Players Loaded': self.Added,
            'Players Removed': self.Removed,
            'Players Updated': self.Updated
        }