class Player(object):
    """
        Player object is a collection properties describing a football player
    """

    def __init__(self, firstName: str, lastName: str, preferredFoot: str, nickName: str = None, id: str = None):
        """

        :param firstName:
        :param lastName:
        :param preferredFoot:
        :param nickName:
        """
        self.__Id = id
        self.__FirstName = self._to_camel_case(firstName)
        self.__LastName = self._to_camel_case(lastName)
        self.__PreferredFoot = preferredFoot
        self.__NickName = self._to_camel_case(nickName)

    def __repr__(self):
        title = '{0}_{1}'.format(self.__FirstName, self.__LastName)
        return '<Player %r>' % title

    def getProperties(self):
        properties = {
            'FirstName': self.__FirstName,
            'LastName': self.__LastName,
            'PreferredFoot': self.__PreferredFoot,
            'NickName': self.__NickName,
        }

        if self.__Id:
            properties['Id'] = self.__Id

        return properties

    def set_id(self, id):
        self.__Id = id

    def get_id(self):
        return self.__Id

    def _to_camel_case(self, value: str) -> str:
        if value:
            return value[0].upper() + value[1:].lower()
        return value