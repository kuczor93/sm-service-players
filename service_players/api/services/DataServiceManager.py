from bson import ObjectId

from service_players.app import db as dbManager
from service_players.api.models.Player import Player


class DataServiceManager(object):

    def __init__(self):
        pass

    def findPlayerById(self, id) -> Player:
        collection = dbManager.db.players
        output = None

        response = collection.find_one({'_id': ObjectId(id)})

        if response:
            output = Player(response['FirstName'], response['LastName'], response['PreferredFoot'],
                            response['NickName'],  response['_id'])

        return output

    def findAllPlayers(self) -> []:
        collection = dbManager.db.players
        output = []

        for record in collection.find():
            output.append(Player(record['FirstName'], record['LastName'], record['PreferredFoot'], record['NickName'],
                                 record['_id']).getProperties())
        return output

    def addPlayer(self, player: Player) -> Player:
        collection = dbManager.db.players
        output = None

        modelHashMap = player.getProperties()
        playerId = collection.insert_one(modelHashMap).inserted_id
        response = collection.find_one({'_id': playerId})

        if response:
            output = Player(response['FirstName'], response['LastName'], response['PreferredFoot'],
                            response['NickName'], response['_id'])
        return output

    def updatePlayer(self, player: Player) -> bool:
        collection = dbManager.db.players

        player_json = player.getProperties()

        # NOTE: _Id is immutable and needs to be removed from properties before update
        p_Id = player_json.pop('Id', None)

        response = collection.update_one(
            {
                '_id': ObjectId(p_Id)
            },
            {
                '$set': player_json
            })

        if response:
            return True

        return False

    def removePlayer(self, id: str) -> bool:
        collection = dbManager.db.players

        response = collection.delete_one({'_id': ObjectId(id)})

        if response and response.deleted_count == 1:
            return True

        return False

    def updatePlayerCollection(self, players: []):
        collection = dbManager.db.players

        collection.delete_many({})

        result_Ids = collection.insert_many(players)

        if result_Ids:
            return result_Ids.inserted_ids
        else:
            return []