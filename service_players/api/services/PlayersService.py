from service_players.api.models.Player import Player
from service_players.api.models.PlayersResponse import PlayersResponse

class PlayersService(object):
    def __init__(self, db):
        self.dsManager = db

    def find(self, id: str) -> Player:
        return self.dsManager.findPlayerById(id)

    def findAll(self) -> list:
        return self.dsManager.findAllPlayers()

    def add(self, data) -> Player:
        if not isinstance(data, Player):
            try:
                player = self.__generatePlayer(data)
            except Exception:
                raise AttributeError('Cannot generate player instance')
        else:
            player = data

        return self.dsManager.addPlayer(player)

    def addMany(self, data: []) -> list:
        return [self.add(p) for p in data]

    def update(self, data: {}) -> bool:
        if not isinstance(data, Player):
            try:
                player = self.__generatePlayer(data)
            except Exception:
                raise AttributeError('Cannot generate player instance')
        else:
            player = data

        return self.dsManager.updatePlayer(player)

    def remove(self, id: str) -> bool:
        return self.dsManager.removePlayer(id)

    def updateCollection(self, collection: []):
        return self.dsManager.updatePlayerCollection(collection)
        # id_collection_response = []
        #
        # if not collection:
        #     raise ValueError('No data specified')
        #
        # try:
        #     player_repository = self.findAll()
        # except Exception:
        #     raise LookupError('Cannot obtain list of existing players')
        #
        # if not player_repository:
        #     for player in collection:
        #         id_collection_response.append(self.add(player).get_id())
        #     return id_collection_response
        #
        # try:
        #     player_repr = '{}_{}'
        #     unique_records = [player for player in collection if
        #                       player_repr.format(player['FirstName'], player['LastName']) not in player_repository]
        # except Exception:
        #     raise ArithmeticError('Could not retrieve unique records from data file')
        #
        # id_collection_response.extend([player.get_id() for player in collection if
        #                       player_repr.format(player['FirstName'], player['LastName']) in player_repository])
        #
        # for p in unique_records:
        #     id_collection_response.append(self.add(p).get_id())
        #
        # return id_collection_response

    def __generatePlayer(self, data: {}) -> Player:
        player = Player(data['FirstName'], data['LastName'], data['PreferredFoot'], data['NickName'])

        if 'Id' in data:
            player.set_id(data['Id'])

        return player