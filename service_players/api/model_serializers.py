from werkzeug.datastructures import FileStorage

from service_players.api.restplus import api
from flask_restplus import fields

playerModel = api.model('Player Instance', {
    'FirstName': fields.String(required=True, description='First name of a player'),
    'LastName': fields.String(required=True, description='Last name of a player'),
    'PreferredFoot': fields.String(required=True, description='Preferred foot of a player'),
    'NickName': fields.String(description='Player\'s selected nickname')
})

playerModels = api.inherit('Collection of Player Instances', {
    'Players': fields.List(fields.Nested(playerModel))
})