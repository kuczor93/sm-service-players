import logging

from flask_restplus import Resource
from service_players.api.JSONEncoder import JSONEncoder
from flask import request, Response

from service_players.api.model_serializers import playerModel, playerModels
from service_players.api.restplus import api
from service_players.api.services import DataServiceManager
from service_players.api.services.PlayersService import PlayersService

log = logging.getLogger(__name__)
ns = api.namespace('players', description='Controller defining CRUD operations on Player Repository')


@ns.route('/')
class PlayerCollection(Resource):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        db = DataServiceManager.DataServiceManager()
        self.playerService = PlayersService(db)
        self.jsonEncoder = JSONEncoder()

    @api.response(200, 'Returns list of all available players')
    @api.response(204, 'No Players available in the repository')
    def get(self):
        """
        Returns list of all players available in the database
        :return: List<Player>
        """
        try:
            response = self.playerService.findAll()

            if response:
                return Response(self.jsonEncoder.encode(response), mimetype='application/json', status=200)
            return Response([], mimetype='application/json', status=204)
        except Exception as err:
            return Response(err, mimetype='application/json', status=500)

    @api.expect(playerModel)
    @api.response(201, 'New player instance successfully created')
    @api.response(400, 'Bad data request')
    def post(self):
        """
        Creates and inserts player instance into the database
        :return: Player
        """

        if not request.json:
            return Response('No data provided', mimetype='application/json', status=400)

        try:
            response = self.playerService.add(request.json)

            if response:
                return Response(self.jsonEncoder.encode(response.getProperties()),
                                mimetype='application/json', status=201)
            return Response([], mimetype='application/json', status=204)
        except Exception as err:
            return Response(err, mimetype='application/json', status=500)

    @api.expect(playerModels)
    @api.response(201, 'Players database successfully updated')
    @api.response(400, 'Bad data request')
    def put(self):
        """
        Updates player instance using provided player Id
        :return: PlayerResponse
        """
        if not request.json or not request.json['Players']:
            return Response('No data provided', mimetype='application/json', status=400)

        try:
            response = self.playerService.updateCollection(request.json['Players'])
        except Exception as err:
            return Response(err, mimetype='application/json', status=500)

        return Response(self.jsonEncoder.encode(response), mimetype='application/json', status=201)

@ns.route('/<string:id>')
class PlayerItem(Resource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        db = DataServiceManager.DataServiceManager()
        self.playerService = PlayersService(db)
        self.jsonEncoder = JSONEncoder()

    @api.response(200, 'Returns a player using provided player ID')
    @api.response(404, 'Player with provided ID does not exist in the repository')
    def get(self, id: str):
        """
        Returns player instance based on provided player Id
        :param id: unique player id
        :return: Player
        """
        response = self.playerService.find(id)

        if response:
            return Response(self.jsonEncoder.encode(response), mimetype='application/json', status=200)
        return Response(None, mimetype='application/json', status=404)

    @api.expect(playerModel)
    def put(self, id: str):
        """
        Updates player instance based on provided player Id
        :param id: unique player id
        :return: StatusCode
        """
        if 'Id' not in request.json:
            request.json['Id'] = id

        response = self.playerService.update(request.json)

        if response:
            return Response(None, mimetype='application/json', status=200)

        return Response('Issue with a player update', mimetype='application/json', status=500)

    def delete(self, id: str):
        """
        Deletes player instance based on provided player id
        :param id: unique player id
        :return: StatusCode
        """
        deleted = self.playerService.remove(id)

        if deleted:
            return Response(None, mimetype='application/json', status=204)
        return Response('Issue with a player delete', mimetype='application/json', status=500)