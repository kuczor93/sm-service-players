import json
from bson import ObjectId

from service_players.api.models.Player import Player
from service_players.api.models.PlayersResponse import PlayersResponse


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        elif isinstance(o, Player):
            return o.getProperties()
        elif isinstance(o, PlayersResponse):
            return o.__repr__()
        return json.JSONEncoder.default(self, o)
