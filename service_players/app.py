import argparse
import logging.config

import sys
sys.path.append('/root/sm-service-players/')

from flask import Flask, Blueprint
from flask_cors import CORS
from flask_pymongo import PyMongo

import service_players.api.controllers.PlayersController
from service_players import settings
from service_players.api.restplus import api
from os import path

app = Flask(__name__)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
log_path = path.join(path.dirname(path.abspath(__file__)), 'logging.conf')
logging.config.fileConfig(log_path)
log = logging.getLogger(__name__)

########## ARG PARSER ##########
arg_parser = argparse.ArgumentParser(description='Defines launch options')
arg_parser.add_argument('--prod', dest='production', action='store_true', help='enabling production mode')

args = arg_parser.parse_args()
################################

def configure_app(flask_app: Flask):
    if 'production' in args and args.production:
        flask_app.config['SERVER_NAME'] = settings.FLASK_SERVER_NAME_PROD
    else:
        flask_app.config['SERVER_NAME'] = settings.FLASK_SERVER_NAME_DEV

    flask_app.config['SWAGGER_UI_DOC_EXPANSION'] = settings.RESTPLUS_SWAGGER_UI_DOC_EXPANSION
    flask_app.config['RESTPLUS_VALIDATE'] = settings.RESTPLUS_VALIDATE
    flask_app.config['RESTPLUS_MASK_SWAGGER'] = settings.RESTPLUS_MASK_SWAGGER
    flask_app.config['ERROR_404_HELP'] = settings.RESTPLUS_ERROR_404_HELP

    flask_app.config['MONGO_DBNAME'] = settings.MONGODB_DBNAME
    flask_app.config['MONGO_URI'] = settings.MONGODB_URI


def initialize_app(flask_app: Flask):

    blueprint = Blueprint('api', __name__, url_prefix='/api')
    api.init_app(blueprint)
    api.add_namespace(service_players.api.controllers.PlayersController.ns)
    flask_app.register_blueprint(blueprint)

configure_app(app)
db = PyMongo(app)

if __name__ == '__main__':
    initialize_app(app)
    log.info('>>>>> Starting development server at http://{}/api/ <<<<<'.format(app.config['SERVER_NAME']))
    app.run()