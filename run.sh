#!/bin/sh

echo "Killing existing python microservice"
pkill -f python3.6

cwd=`pwd`
cd /root/sm-service-players

echo "Stashing uncommitted changes"
git stash

echo "Updating Repo..."
git reset --hard HEAD
git pull

echo "Updating requirements..."
/usr/local/bin/pip3.6 install -r /root/sm-service-players/requirements.txt

echo "Restarting python microservice"
nohup /usr/local/bin/python3.6 /root/sm-service-players/service_players/app.py --prod &

echo "Returning to previous location"
cd $cwd